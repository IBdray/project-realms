// Copyright 2022 Ivan Babanov, IB Dray. No Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECTREALMS_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxHealth;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float CurrentHealth;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bDead;
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	void ReduceHealth(float Amount);
	UFUNCTION(BlueprintCallable)
	void IncreaseHealth(float Amount);
	
	UFUNCTION(BlueprintCallable)
	float GetMaxHealth() const {return MaxHealth;}
	UFUNCTION(BlueprintCallable)
	float GetCurrentHealth() const {return CurrentHealth;}
	UFUNCTION(BlueprintCallable)
	float IsDead() const {return bDead;}

		
};
