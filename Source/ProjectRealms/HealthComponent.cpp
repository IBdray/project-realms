// Copyright 2022 Ivan Babanov, IB Dray. No Rights Reserved.


#include "HealthComponent.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	MaxHealth = 200.0f;
	CurrentHealth = MaxHealth;
	// ...
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UHealthComponent::ReduceHealth(float Amount)
{
	CurrentHealth = (Amount > CurrentHealth) ? 0.0f : CurrentHealth - Amount;
	if (CurrentHealth == 0.0f)
	{
		bDead = true;
	}
}

void UHealthComponent::IncreaseHealth(float Amount)
{
	if (!bDead)
	{
		CurrentHealth = (Amount + CurrentHealth > MaxHealth) ? MaxHealth : CurrentHealth;
	}
}


