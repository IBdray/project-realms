// Copyright 2022 Ivan Babanov, IB Dray. No Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ProjectRealmsCharacter.generated.h"

class UHealthComponent;


UCLASS(config=Game)
class AProjectRealmsCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AProjectRealmsCharacter();

	UFUNCTION(BlueprintCallable)
	void ReceiveDamage(float Amount);

	UFUNCTION(BlueprintImplementableEvent)
	void OnTakeDamage();
	UFUNCTION(BlueprintImplementableEvent)
	void OnDeath();
	
protected:
	virtual void BeginPlay();

	
public:
	
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UHealthComponent* Health;

};

