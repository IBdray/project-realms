// Copyright 2022 Ivan Babanov, IB Dray. No Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "ProjectRealmsCharacter.h"
#include "RealmsEnemyCharacter.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTREALMS_API ARealmsEnemyCharacter : public AProjectRealmsCharacter
{
	GENERATED_BODY()
	
};
