// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ProjectRealmsGameMode.generated.h"

UCLASS(minimalapi)
class AProjectRealmsGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AProjectRealmsGameMode();
};



