// Copyright 2022 Ivan Babanov, IB Dray. No Rights Reserved.


#include "AdvancedMovementComponent.h"

UAdvancedMovementComponent::UAdvancedMovementComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{}


void UAdvancedMovementComponent::InitializeComponent()
{
	Super::InitializeComponent();
}

void UAdvancedMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType,
	FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}
