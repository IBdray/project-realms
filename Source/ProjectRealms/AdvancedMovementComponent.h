// Copyright 2022 Ivan Babanov, IB Dray. No Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "AdvancedMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTREALMS_API UAdvancedMovementComponent : public UCharacterMovementComponent
{
	GENERATED_UCLASS_BODY()

protected:
	//Init
	virtual void InitializeComponent() override;
	
	//Tick
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;
	
};
