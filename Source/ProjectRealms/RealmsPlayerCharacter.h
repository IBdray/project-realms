// Copyright 2022 Ivan Babanov, IB Dray. No Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "ProjectRealmsCharacter.h"
#include "RealmsPlayerCharacter.generated.h"

class UInputComponent;
class USkeletalMeshComponent;
class USceneComponent;
class UCameraComponent;
class UAnimMontage;
class USoundBase;

// Declaration of the delegate that will be called when the Primary Action is triggered
// It is declared as dynamic so it can be accessed also in Blueprints
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnUseItem);
/**
 * 
 */
UCLASS()
class PROJECTREALMS_API ARealmsPlayerCharacter : public AProjectRealmsCharacter
{
	GENERATED_BODY()

public:
	ARealmsPlayerCharacter();

protected:
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	
	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);
	
	void TurnAtRate(float AxisValue);
	void LookUpAtRate(float AxisValue);

	void OnPrimaryAction();
	

public:
	USkeletalMeshComponent* GetFPMesh() const { return FPMesh; }
	UCameraComponent* GetFPCamera() const { return FPCamera; }

	
	UFUNCTION(BlueprintImplementableEvent)
	void OnFire();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FPCamera;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Mesh")
	USkeletalMeshComponent* FPMesh;

public:
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float TurnRateGamepad;

	/** Delegate to whom anyone can subscribe to receive this event */
	UPROPERTY(BlueprintAssignable, Category = "Interaction")
	FOnUseItem OnUseItem;
};
