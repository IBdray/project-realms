// Copyright 2022 Ivan Babanov, IB Dray. No Rights Reserved.

#include "ProjectRealmsCharacter.h"

#include "HealthComponent.h"


AProjectRealmsCharacter::AProjectRealmsCharacter()
{
	Health = CreateDefaultSubobject<UHealthComponent>(TEXT("Health"));

}

void AProjectRealmsCharacter::ReceiveDamage(float Amount)
{
	Health->ReduceHealth(Amount);
	OnTakeDamage();

	if (Health->IsDead())
	{
		OnDeath();
	}
}


void AProjectRealmsCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();
}

